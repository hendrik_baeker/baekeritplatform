const elixir = require('laravel-elixir');

require('laravel-elixir-vue');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
elixir(function(mix){
   mix.styles([
       'bootstrap.min.css',
       'mdb.min.css',
       'font-awesome.min.css',
       'style.css',
   ]);
    mix.scripts([
        'jquery-2.2.3.min.js',
        'bootstrap.min.js',
        'mdb.min.js',
        'tether.min.js',
        'custom.js',
    ]);
    mix.styles(['bootstrap.min.css'], 'public/css/bootstrap.css')
        .styles(['mdb.min.css'], 'public/css/mdb.css')
        .styles(['style.css'], 'public/css/style.css');
    mix.scripts(['jquery-2.2.3.min.js'], 'public/js/jquery.js')
        .scripts(['bootstrap.min.js'], 'public/js/bootstrap.js')
        .scripts(['mdb.min.js'], 'public/js/mdb.js')
        .scripts(['tether.min.js'], 'public/js/tether.js')
        .scripts(['custom.js'], 'public/js/custom.js')
        .scripts(['adsense.js'], 'public/js/adsense.js');
    mix.sass([
        'app.scss',
        'mdb.scss',
    ]);
    mix.browserSync({
        proxy: 'project.dev'
    });
    mix.version('css/all.css');
    mix.version('js/all.js');
});