<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::get('/', 'Controller@initializer');
Route::get('sitemap.xml', 'SitemapController@generate');
// Registration Routes...
Route::get('/register', 'Auth\RegisterController@showRegistrationForm');
Route::post('/register', 'Auth\RegisterController@register');
// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
Route::post('page/contact', 'ContactFormController@sendContactForm');
Route::post('feedback', 'ContactFormController@sendFeedback');
Route::post('register', 'Auth\RegisterController@register');
Route::post('logout', 'Auth\LoginController@logout');
Route::group(['prefix' => 'admin'], function(){
    Route::get('login', 'Controller@showLogin');
    Route::post('page/frontpage', 'AdminPagesController@saveFrontPage');
    Route::post('page/create', 'AdminPagesController@savePage');
    Route::post('menu/create', 'MenuController@saveMenu');
    Route::post('general', 'GeneralInformationController@saveGeneralInformations');
    Route::post('categories', 'ProductCategoriesController@createCategory');
    Route::post('products', 'ProductsController@createProduct');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout');
});
Route::get('{module?}/{action?}/{id?}/{slug?}', 'Controller@initializer');
Route::get('/home', 'PagesController@showPage');
