<?php

namespace App\Http\Controllers;

use App\contents;
use App\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use View;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Artisan;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    //Defining User Roles
    const IS_ADMIN = 1;

    //Definung Page Types

    const IS_STANDARD = 0;
    const IS_SHOP = 1;
    const IS_BLOG = 2;

    static $template;

    static $page_type = array(
        0 => 'Standard',
        1 => 'Shop',
        2 => 'Blog'
    );

    static $frontpage_sections = array(
        'section_intro_enabled',
        'section_features_enabled',
        'section_pricing_enabled'
    );

    public $user;

    public $plugins;

    public $item_id;

    private $id;

    public function __construct()
    {
        if (!Auth::check()) {
            if (count(explode(".", $_SERVER['HTTP_HOST'])) == 2) {
                $this->user = User::where('id', '=', 1)->first();
            } else {
                if (User::where('name', '=', explode(".", $_SERVER['HTTP_HOST'])[0])->first() == null) {
                    $this->user = User::where('id', '=', 1)->first();
                }
                $this->user = User::where('name', '=', explode(".", $_SERVER['HTTP_HOST'])[0])->first();
            }
        }
        return User::find(1);
    }

    public static function loadContactInformation()
    {
        View::share('contact_information', GeneralInformationController::getGeneralInformations());
        return;
    }

    public static function getCurrentTemplate()
    {
        return GeneralInformationController::getGeneralInformations()->template;
    }

    public static function loadContents($front_page = false)
    {
        if (!$front_page) {
            $contents = contents::where('user_id', '=', self::getUser()->id)->get();
            view()->share('page', $contents);
        } elseif ($front_page) {
            view()->share('page',
                contents::where('is_frontpage', '=', 1)->where('user_id', '=', self::getUser()->id)->first());
        }
        return;
    }

    public static function getUser()
    {
        if (!Auth::check()) {
            if (count(explode(".", $_SERVER['HTTP_HOST'])) == 2) {
                return User::where('id', '=', 1)->first();
            }
            if (count(explode(".", $_SERVER['HTTP_HOST'])) == 3) {
                return User::where('name', '=', explode(".", $_SERVER['HTTP_HOST'])[0])->first();
            }
            return Auth::User();
        } else {
            if (count(explode(".", $_SERVER['HTTP_HOST'])) == 3) {
                return User::where('name', '=', explode(".", $_SERVER['HTTP_HOST'])[0])->first();
            }
            return Auth::User();
        }
    }

    public static function generateLinks($val = false, $reverse = false)
    {
        if (!$val) {
            return null;
        }
        $search = [
            ' ',
            '/'
        ];
        $replace = [
            '-',
            '_'
        ];
        if (!$reverse) {
            return str_replace($search, $replace, $val);
        } else {
            return str_replace($replace, $search, $val);
        }
    }

    public static function ReplaceIntWithEmptyStringsInArray($array = false)
    {
        if (!$array) {
            return false;
        }
        foreach ($array as $key => $value) {
            if (is_int($value)) {
                $array[$key] = '';
            }
        }
        return $array;
    }

    public function initializer($module = false, $action = false, $id = false, $slug = false)
    {
        if ($module == "install") {
            return $this->install();
        }
        $this->checkUserIfIsAdmin();
        $this->user = self::getUser();
        self::$template = GeneralInformationController::getGeneralInformations()->template;
        View()->share('contact_information', GeneralInformationController::getGeneralInformations());
        if ($id) {
            $this->id = $id;
            View::share('id', $this->id);
        }
        View::share('controller', $module);
        View::share('action', $action);
        switch ($module) {
            case false:
                return PagesController::showPage();
            case 'page':
                return PagesController::showPage($action);
            case 'admin':
                if (!Auth::check()) {
                    return view('auth.login');
                }
                switch($action)
                {
                    case false:
                        return AdminController::showAdminIndex();
                    default:
                        if(method_exists($this, $action))
                        {
                            if ($id)
                            {
                                if ($slug) {
                                    $this->item_id = $slug;
                                }
                                return $this->$action($id);
                            }
                            return $this->$action();
                        }else{
                            return self::requestedPageNotFound($action);
                        }
                }
            default:
                return PagesController::showPage();
        }
    }


    public function install()
    {
        if (Schema::hasTable('frontpages')) {
            return PagesController::showPage();
        }
        Artisan::call('migrate');
        return AdminController::installation();
    }

    public function checkUserIfIsAdmin()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            if (isset($this->user->role) AND $this->user->role == self::IS_ADMIN) {
                view()->share('is_admin', true);
            }
            return $next($request);
        });
    }

    private function requestedPageNotFound($module)
    {
        return view('errors.404_admin')
            ->with('module', $module);
    }

    public function showLogin()
    {
        return view('auth.login');
    }

    private function page($action = null)
    {
        return AdminPagesController::switch ($action, $this->item_id);
    }

    private function menu($action = false)
    {
        return MenuController::switch ($action);
    }

    private function blog($action = false, $id = false)
    {
        return PostsController::switch ($action, $id);
    }

    private function user($action = null)
    {
        return UserController::switch ($action);
    }

    private function generalinformation($action = null)
    {
        return GeneralInformationController::setGeneralInformations();
    }

    private function register()
    {
        return view('auth.register');
    }

    private function dashboard()
    {
        return view('admin.index');
    }
}
