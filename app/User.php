<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function get_user()
    {
        return $this->belongsTo('App\contents');
    }

    public function get_user_for_post()
    {
        return $this->belongsTo('App\posts');
    }

    public function get_user_for_comment()
    {
        return $this->belongsTo('App\comments');
    }
}
